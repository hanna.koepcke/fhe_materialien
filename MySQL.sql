CREATE DATABASE IF NOT exists exercise;

USE exercise;

CREATE TABLE person (
    id int PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL
);

INSERT INTO person (first_name,last_name) VALUES ('Erika', 'Musterfrau');
INSERT INTO person (first_name,last_name) VALUES ('Max', 'Mustermann');


CREATE TABLE identity_card (
    id int PRIMARY KEY AUTO_INCREMENT,
    issue_date DATE NOT NULL,
    expiry_date DATE NOT NULL,
    person_id int UNIQUE,
    FOREIGN KEY (person_id) REFERENCES person(id)
);

INSERT INTO identity_card (issue_date,expiry_date,person_id) VALUES ('2021-07-01', '2031-07-01', 1);

CREATE TABLE lecturer (
    id int PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL
);

CREATE TABLE lecture (
    id int PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    lecturer_id int,
    FOREIGN KEY (lecturer_id) REFERENCES person(id)
);

INSERT INTO lecturer (first_name,last_name) VALUES ('Erika', 'Musterfrau');
INSERT INTO lecturer (first_name,last_name) VALUES ('Max', 'Mustermann');
INSERT INTO lecture(name, lecturer_id) VALUES ('NoSQL Datenbanken',1);
INSERT INTO lecture(name, lecturer_id) VALUES ('Data Mining',1);

CREATE TABLE student (
    id int PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL
);

CREATE TABLE enrollment (
    id int PRIMARY KEY AUTO_INCREMENT,
    lecture_id int,
    student_id int,
    FOREIGN KEY (lecture_id) REFERENCES lecture(id),
    FOREIGN KEY (student_id) REFERENCES student(id)
);

INSERT INTO student (first_name,last_name) VALUES ('Erika', 'Musterfrau');
INSERT INTO student (first_name,last_name) VALUES ('Max', 'Mustermann');
INSERT INTO enrollment(lecture_id,student_id) VALUES (1,1);
INSERT INTO enrollment(lecture_id,student_id) VALUES (2,1);
INSERT INTO enrollment(lecture_id,student_id) VALUES (2,2);