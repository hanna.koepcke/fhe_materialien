# FHE_Materialien


## Plugin installation for Visual Studio Code

Install the following extensions:

- Docker https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker
- MySQL https://marketplace.visualstudio.com/items?itemName=formulahendry.vscode-mysql
- MongoDB https://marketplace.visualstudio.com/items?itemName=mongodb.mongodb-vscode



## Database installation

Install mySQL and mongodb via docker

```
docker run --name mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -p 3306:3306 -d mysql:latest --default-authentication-plugin=mysql_native_password
docker run --name mongo -p 27017:27017 -d mongo:latest
```

